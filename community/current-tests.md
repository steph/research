## Open User Research
In this page you find the current needs to be run when testing Tor products, as well as methodologies and testing scripts. Before running a Tor user research, be sure you read our [Guidelines to be a User Researcher with Tor](#).

### Tor Browser for Desktop
* [Usability Testing: Onboarding](https://link)
* [Usability Testing: Security Settings](https://link)
* [Usability Testing: New Identity](https://link)
* [Usability Testing: Warnings and Notifications](https://link)
* [Usability Testing: Circuit Display](https://link)

### Tor Browser for Android
* [Usability Testing: Onboarding](https://link)
* [Usability Testing: Security Settings](https://link)
* [Usability Testing: New Identity](https://link)
* [Usability Testing: Warnings and Notifications](https://link)

### Onion Services
* [Usability Testing: Onion Security Indicator](https://trac.torproject.org/projects/tor/attachment/wiki/org/teams/UxTeam/UserResearch/User%20testing%20.onion%20states%20Test.pdf)
* [Usability Methodology: Onion Security Indicator](https://trac.torproject.org/projects/tor/attachment/wiki/org/teams/UxTeam/UserResearch/User%20testing%20.onion%20states%20icons%20Methodology.pdf)

### Circuit Display
* [Usability Testing: Circuit Display](https://trac.torproject.org/projects/tor/attachment/wiki/org/teams/UxTeam/UserResearch/User%20testing%20circuit%20display%20Test.pdf)
* [Usability Methodology: Circuit Display](https://trac.torproject.org/projects/tor/attachment/wiki/org/teams/UxTeam/UserResearch/User%20testing%20circuit%20display%20Methodology.pdf)

## Past User Research
On our commitment with an open design, we make public here our user research ran through the global-south. If you want to run user research with us, please [get in touch.](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux)

| Project  | Methodology | Locations | Dates | Reporting |
| -------- | ----------- | --------- | --------- | ----- |
| Tor Launcher | Usability testing (.pdf) | Mumbai(IN) | Q118 | .pdf |
| Onion Security Indicator | Usability testing (.pdf) | Mumbai(IN), Kampala(UG), Valencia(ES), Mombasa(KE)| Q118, Q218 | .pdf |
| TB Circuit Display | Usability testing (.pdf) | Kampala(UG), Nairobi(KE), Mombasa(KE) | Q118, Q218 | .pdf |
| Tor Browser for Desktop | User needs discovery (.pdf) | Bogotá(CL), Cali(CL), Valle del Cauca(CL), Kampala(UG), Hoima(UG), Nairobi(KE)  | 2018 | .pdf |
| Tor Browser for Android | User needs discovery (.pdf) | Bogotá(CL), Cali(CL), Valle del Cauca(CL), Kampala(UG), Hoima(UG), Nairobi(KE)  | 2018 | .pdf |
| Tor Users Demographics | Survey (link) | Online  | 2018 | .pdf |
