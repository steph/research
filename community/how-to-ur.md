## How to do product tests for Tor

Usability is about making sure anyone, no matter their technical background, can use a tool. Our approach to usability is built on respecting and safeguarding the privacy of our users.  We test our hypotheses and make observations in the safest way possible; in most cases, we do this work in-person with our users, not by collecting data about their behavior.

### Guidelines to be a User Research for Tor < link
Do you want to be a User Research for Tor? Learn how to do so respecting the privacy and security of our users.

### Get to know our tests < link
These resources will give you the basics about our services.

### Read our F.A.Q. < link
You might get a gazillion questions about Tor during the interview, find out which are the most frequent ones and get prepared.

### Talk to us
You can reach the UX team by joining our [mailing list](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux) and our [IRC channel](https://www.torproject.org/contact/#irc). 

